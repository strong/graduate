<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;
use think\Request;
use app\common\model\Category as CategoryModel;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    /**
     * Article模型对象
     * @var \app\admin\model\Article
     */
    protected $Article = null;
    protected $Rotation = null;

    public function _initialize()
    {         
        parent::_initialize();
        $this->Article = new \app\admin\model\Article;
        $this->Rotation = new \app\admin\model\Rotation;

    }
    /**
     * 首页
     */
    public function index()
    {
        $where = '';
        $sort = '';
        $order = '';
        list($offset) = '';  
        $limit = 10;

        //文章 所长简介
        $Article_suo_list = $this->Article
            ->field("id,cid,title,author,desc,position,image,description") 
            ->where(['cid'=>14])
            ->order($sort, $order)
            ->limit(1)
            ->select(); 
        $Article_suo_list = collection($Article_suo_list)->toArray();

        //文章 新闻动态
        $Article_news_list = $this->Article
            ->field("id,cid,title")
            ->where(['cid'=>15])
            ->order($sort, $order)
            ->limit($offset, $limit)
            ->select(); 
        $Article_news_list = collection($Article_news_list)->toArray();

        //文章 科研动态
        $Article_keyan_list = $this->Article
            ->field("id,cid,title") 
            ->where(['cid'=>16])
            ->order($sort, $order)
            ->limit($offset, $limit)
            ->select(); 
        $Article_keyan_list = collection($Article_keyan_list)->toArray();
  
        //轮播图
        $Rotation_list = $this->Rotation
            ->field("id,cid,name,image,url")
            ->where(['cid'=>17])
            ->order($sort, $order)
            ->limit(10)
            ->select(); 
        $Rotation_list = collection($Rotation_list)->toArray();

        //新闻动态图
        $Rotation_news_list = $this->Rotation
            ->field("id,cid,name,image,url")
            ->where(['cid'=>15])
            ->order($sort, $order)
            ->limit(1)
            ->select(); 
        $Rotation_news_list = collection($Rotation_news_list)->toArray();
 
        $result = array(
            "article_suo_list" => $Article_suo_list,
            "article_news_list" => $Article_news_list,
            "article_keyan_list" => $Article_keyan_list, 
            "rotation_list" => $Rotation_list,
            "Rotation_news_list" => $Rotation_news_list,
        );
        $this->success('请求成功',$result);
    }
    /**
     * 文章详情
     * 能获取所有分类的详情
     */
    public function detail($id)
    { 
        $detail = $this->Article
            ->where(['id'=>$id])
            ->find(); 

        $cid_list = $this->Article
            ->field("id,title")
            ->where(['cid'=>$detail['cid']])
            ->select();
        foreach ($cid_list as $key => $value) {
            if($value['id'] == $detail['id']){
                $up = $cid_list[$key-1]??'';
                $next = $cid_list[$key+1]??'';
            }
        }

        $result = array(
            "detail" => $detail,
            "up" => $up,
            "next" => $next,
        );
        $this->success('请求成功',$result);
    }
    /**
     * 首页-单页分类
     */
    public function get_category()
    { 
        $typeList = CategoryModel::where(['type'=>'page','status'=>'normal'])->order('weigh','asc')->select();
        
        $result = array(
            "detail" => $typeList,
        );
        $this->success('请求成功',$result);
    }

    
    /**
     * 首页-单页分类-详情
     * 导航详情 跳转对应列表 新闻动态15  科研动态14
     */
    public function get_cate_detail($id)
    { 
        $offset = $this->request->get("offset", 1);
        $limit = $this->request->get("limit", 10);
        $offset = ceil($limit*($offset-1));
        // print_r($offset);die;
        $detail = $this->Article
            ->where(['cid'=>$id])
            ->order('weigh','desc')
            ->limit($offset, $limit)
            ->select();
        $totle = $this->Article
            ->where(['cid'=>$id])
            ->count('id');
        
        $result = array(
            'totle' => $totle,
            'list' => $detail,
            "detail" => array_shift($detail),
        );
        $this->success('请求成功',$result);
    }
}
