<?php

namespace app\common\model;

use think\Model;

/**
 * 会员积分日志模型
 */
class Article Extends Model
{

    // 表名
    protected $name = 'article'; 
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = '';
    // 追加属性
    protected $append = [
    ];
}
