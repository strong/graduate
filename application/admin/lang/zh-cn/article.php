<?php

return [
    'Cid'         => '分类ID',
    'Type'        => '栏目类型',
    'Desc'        => '简介',
    'Position'    => '职位',
    'Image'       => '图片',
    'Description' => '内容',
    'Diyname'     => '自定义名称',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间',
    'Weigh'       => '权重排序',
    'Status'      => '状态'
];
