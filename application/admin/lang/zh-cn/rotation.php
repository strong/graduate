<?php

return [
    'Cid'        => '分类ID',
    'Name'       => '名称',
    'Image'      => '地址',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Weigh'      => '权重',
    'Status'     => '状态'
];
