/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : fast

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/09/2020 19:27:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fa_admin
-- ----------------------------
DROP TABLE IF EXISTS `fa_admin`;
CREATE TABLE `fa_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `logintime` int(10) NULL DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_admin
-- ----------------------------
INSERT INTO `fa_admin` VALUES (1, 'admin', 'Admin', '8c808484fceb4b7623321ba12391e683', 'b7fd10', '/uploads/20200829/3ab04b7b18bc70c20bde7e72d51be817.jpg', 'admin@admin.com', 0, 1599537226, '127.0.0.1', 1492186163, 1599537226, '79746eba-749a-40cb-ba7f-f6f8b1d5bf84', 'normal');

-- ----------------------------
-- Table structure for fa_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_admin_log`;
CREATE TABLE `fa_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_admin_log
-- ----------------------------
INSERT INTO `fa_admin_log` VALUES (1, 0, 'Unknown', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"7cb3a9a22b56f6ba89f68ffaebfa32ae\",\"username\":\"root\",\"captcha\":\"uj32\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1597231834);
INSERT INTO `fa_admin_log` VALUES (2, 0, 'Unknown', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"99c3b13aafe059d5ffa03f42353cb025\",\"username\":\"root\",\"captcha\":\"pd6n\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1597231845);
INSERT INTO `fa_admin_log` VALUES (3, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"a78ff4e326e9d183631bf0367d68cd88\",\"username\":\"admin\",\"captcha\":\"4bw3\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1597231858);
INSERT INTO `fa_admin_log` VALUES (4, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"350a05ce7430610426d26b8b0a041ac2\",\"username\":\"admin\",\"captcha\":\"wcvg\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1597232427);
INSERT INTO `fa_admin_log` VALUES (5, 0, 'Unknown', '/iVJRvLGpsj.php/index/login', '', '{\"__token__\":\"4955e1099d52392c61c7fa494e4bc3c9\",\"username\":\"admin\",\"captcha\":\"6cdt\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598674620);
INSERT INTO `fa_admin_log` VALUES (6, 1, 'admin', '/iVJRvLGpsj.php/index/login', '登录', '{\"__token__\":\"ac7fd3b11c56190935febfbc78d20d81\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598674635);
INSERT INTO `fa_admin_log` VALUES (7, 1, 'admin', '/iVJRvLGpsj.php/category/del/ids/13', '分类管理 删除', '{\"action\":\"del\",\"ids\":\"13\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598674759);
INSERT INTO `fa_admin_log` VALUES (8, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/1', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"1\",\"params\":\"ismenu=0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598674900);
INSERT INTO `fa_admin_log` VALUES (9, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598674900);
INSERT INTO `fa_admin_log` VALUES (10, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"logo1.png\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598675004);
INSERT INTO `fa_admin_log` VALUES (11, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"9bcb7ddf494bae62842fa11546096b56\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598675031);
INSERT INTO `fa_admin_log` VALUES (12, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/4', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"4\",\"params\":\"ismenu=0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598675063);
INSERT INTO `fa_admin_log` VALUES (13, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598675063);
INSERT INTO `fa_admin_log` VALUES (14, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"\\u9996\\u9875\\u52a8\\u56fe.jpg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676139);
INSERT INTO `fa_admin_log` VALUES (15, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/8', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"8\",\"params\":\"ismenu=0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676427);
INSERT INTO `fa_admin_log` VALUES (16, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676427);
INSERT INTO `fa_admin_log` VALUES (17, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"e9e8ab6065acca9708ced89de249c38b\",\"row\":{\"name\":\"\\u6211\\u7684\\u7f51\\u7ad92\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676459);
INSERT INTO `fa_admin_log` VALUES (18, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"39c86768a640292509ae266483ca24fa\",\"row\":{\"name\":\"\\u6211\\u7684\\u7f51\\u7ad9111\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676475);
INSERT INTO `fa_admin_log` VALUES (19, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"6c9206bf9159b73c9a14e8df4d0f52ac\",\"row\":{\"name\":\"\\u7814\\u7a76\\u6240\\u540e\\u53f0\\u7ba1\\u7406\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676494);
INSERT INTO `fa_admin_log` VALUES (20, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"10db9c8c710586c5c4b59b036b3b27fd\",\"row\":{\"name\":\"\\u540e\\u53f0\\u7ba1\\u7406\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676510);
INSERT INTO `fa_admin_log` VALUES (21, 1, 'admin', '/iVJRvLGpsj.php/user/user/edit/ids/1?dialog=1', '会员管理 会员管理 编辑', '{\"dialog\":\"1\",\"row\":{\"group_id\":\"1\",\"username\":\"admin\",\"nickname\":\"admin\",\"password\":\"\",\"email\":\"admin@163.com\",\"mobile\":\"13888888888\",\"avatar\":\"\\/uploads\\/20200829\\/3ab04b7b18bc70c20bde7e72d51be817.jpg\",\"level\":\"0\",\"gender\":\"0\",\"birthday\":\"2017-04-15\",\"bio\":\"\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2018-01-17 14:28:12\",\"logintime\":\"2018-01-17 14:46:54\",\"loginip\":\"127.0.0.1\",\"loginfailure\":\"0\",\"joinip\":\"127.0.0.1\",\"jointime\":\"2017-04-06 14:50:18\",\"status\":\"normal\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676616);
INSERT INTO `fa_admin_log` VALUES (22, 1, 'admin', '/iVJRvLGpsj.php/general.profile/update', '常规管理 个人资料 更新个人信息', '{\"__token__\":\"b8c4c05a6b413a532bf46ad838ed42ea\",\"row\":{\"avatar\":\"\\/assets\\/img\\/avatar.png\",\"email\":\"admin@admin.com\",\"nickname\":\"Admin\",\"password\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676638);
INSERT INTO `fa_admin_log` VALUES (23, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"\\u9996\\u9875\\u52a8\\u56fe.jpg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676657);
INSERT INTO `fa_admin_log` VALUES (24, 1, 'admin', '/iVJRvLGpsj.php/general.profile/update', '常规管理 个人资料 更新个人信息', '{\"__token__\":\"b1bf4173acab2d96b2352038d4077808\",\"row\":{\"avatar\":\"\\/uploads\\/20200829\\/3ab04b7b18bc70c20bde7e72d51be817.jpg\",\"email\":\"admin@admin.com\",\"nickname\":\"Admin\",\"password\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598676660);
INSERT INTO `fa_admin_log` VALUES (25, 1, 'admin', '/iVJRvLGpsj.php/category/add?dialog=1', '分类管理 添加', '{\"dialog\":\"1\",\"row\":{\"type\":\"default\",\"pid\":\"0\",\"name\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"nickname\":\"suo\",\"image\":\"\\/uploads\\/20200829\\/cc64fc0acc5310e998734a8e75a6b374.png\",\"keywords\":\"jianjie\",\"description\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"weigh\":\"1\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598677028);
INSERT INTO `fa_admin_log` VALUES (26, 1, 'admin', '/iVJRvLGpsj.php/category/add?dialog=1', '分类管理 添加', '{\"dialog\":\"1\",\"row\":{\"type\":\"article\",\"pid\":\"0\",\"name\":\"\\u65b0\\u95fb\\u52a8\\u6001\",\"nickname\":\"news\",\"image\":\"\",\"keywords\":\"news\",\"description\":\"news\",\"weigh\":\"0\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598677137);
INSERT INTO `fa_admin_log` VALUES (27, 1, 'admin', '/iVJRvLGpsj.php/category/add?dialog=1', '分类管理 添加', '{\"dialog\":\"1\",\"row\":{\"type\":\"article\",\"pid\":\"0\",\"name\":\"\\u79d1\\u7814\\u52a8\\u6001\",\"nickname\":\"keyan\",\"image\":\"\",\"keywords\":\"keyan\",\"description\":\"keyan\",\"weigh\":\"0\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598677168);
INSERT INTO `fa_admin_log` VALUES (28, 1, 'admin', '/iVJRvLGpsj.php/category/del/ids/5,7,11,10,6,9,8,2,4,3,1', '分类管理 删除', '{\"action\":\"del\",\"ids\":\"5,7,11,10,6,9,8,2,4,3,1\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685001);
INSERT INTO `fa_admin_log` VALUES (29, 1, 'admin', '/iVJRvLGpsj.php/category/del/ids/12', '分类管理 删除', '{\"action\":\"del\",\"ids\":\"12\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685010);
INSERT INTO `fa_admin_log` VALUES (30, 1, 'admin', '/iVJRvLGpsj.php/category/edit/ids/14?dialog=1', '分类管理 编辑', '{\"dialog\":\"1\",\"row\":{\"type\":\"page\",\"pid\":\"0\",\"name\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"nickname\":\"suo\",\"image\":\"\",\"keywords\":\"jianjie\",\"description\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"weigh\":\"14\",\"status\":\"normal\",\"flag\":[\"\"]},\"ids\":\"14\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685045);
INSERT INTO `fa_admin_log` VALUES (31, 1, 'admin', '/iVJRvLGpsj.php/general.config/edit', '常规管理 系统配置 编辑', '{\"__token__\":\"05978cf10f96e1e223599ae63598fe71\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685145);
INSERT INTO `fa_admin_log` VALUES (32, 1, 'admin', '/iVJRvLGpsj.php/general/config/check', '常规管理 系统配置', '{\"row\":{\"name\":\"gongsi\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685201);
INSERT INTO `fa_admin_log` VALUES (33, 1, 'admin', '/iVJRvLGpsj.php/general.config/add', '常规管理 系统配置 添加', '{\"__token__\":\"024da777891ed4e1dad4c09e242801e7\",\"row\":{\"type\":\"text\",\"group\":\"email\",\"name\":\"gongsi\",\"title\":\"gongsi\",\"value\":\"\\u5317\\u4eac\\u533b\\u836f\\u6559\\u80b2\\u534f\\u4f1a\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"\",\"rule\":\"\",\"extend\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685232);
INSERT INTO `fa_admin_log` VALUES (34, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"\\u9996\\u9875\\u52a8\\u56fe.jpg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685301);
INSERT INTO `fa_admin_log` VALUES (35, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"\\u9996\\u9875\\u56fe.jpg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685312);
INSERT INTO `fa_admin_log` VALUES (36, 1, 'admin', '/iVJRvLGpsj.php/category/edit/ids/16?dialog=1', '分类管理 编辑', '{\"dialog\":\"1\",\"row\":{\"type\":\"article\",\"pid\":\"0\",\"name\":\"\\u79d1\\u7814\\u52a8\\u6001\",\"nickname\":\"keyan\",\"image\":\"\\/uploads\\/20200829\\/2b5f48b8837334ac48170a8175c19e40.jpg\",\"keywords\":\"keyan\",\"description\":\"keyan\",\"weigh\":\"16\",\"status\":\"normal\",\"flag\":[\"\"]},\"ids\":\"16\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598685316);
INSERT INTO `fa_admin_log` VALUES (37, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/add?dialog=1', '权限管理 菜单规则 添加', '{\"dialog\":\"1\",\"__token__\":\"a23e6aca106b828d82071543bec8a80b\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"article\",\"title\":\"\\u6587\\u7ae0\\u7ba1\\u7406\",\"icon\":\"fa fa-paste\",\"weigh\":\"0\",\"condition\":\"\",\"remark\":\"\",\"status\":\"normal\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752914);
INSERT INTO `fa_admin_log` VALUES (38, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752914);
INSERT INTO `fa_admin_log` VALUES (39, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,2,6,7,85,8,3,5,9,10,11,12,4,66,67,73,79\",\"changeid\":\"85\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752930);
INSERT INTO `fa_admin_log` VALUES (40, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752930);
INSERT INTO `fa_admin_log` VALUES (41, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"85,1,2,6,7,8,3,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"85\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752952);
INSERT INTO `fa_admin_log` VALUES (42, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752952);
INSERT INTO `fa_admin_log` VALUES (43, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/edit/ids/85?dialog=1', '权限管理 菜单规则 编辑', '{\"dialog\":\"1\",\"__token__\":\"dfeb306cd36353ba7d6dd0469b57ca94\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"article\",\"title\":\"\\u6587\\u7ae0\\u7ba1\\u7406\",\"icon\":\"fa fa-paste\",\"weigh\":\"1\",\"condition\":\"\",\"remark\":\"\",\"status\":\"normal\"},\"ids\":\"85\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752977);
INSERT INTO `fa_admin_log` VALUES (44, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598752977);
INSERT INTO `fa_admin_log` VALUES (45, 1, 'admin', '/iVJRvLGpsj.php/index/login', '登录', '{\"__token__\":\"1bdd3d1617445775d8fd26831cd1daae\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598754031);
INSERT INTO `fa_admin_log` VALUES (46, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"414d60232b034091e687bcc9845aae49\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598972943);
INSERT INTO `fa_admin_log` VALUES (47, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/1?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"1\",\"type\":\"1\",\"title\":\"\\u6807\\u9898\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\\u963f\\u8428\\u5fb7\",\"position\":\"12312\\u53bb\",\"image\":\"\\/uploads\\/20200829\\/cc64fc0acc5310e998734a8e75a6b374.png\",\"description\":\"1231312\",\"diyname\":\"3123123123\",\"weigh\":\"1\",\"status\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598973166);
INSERT INTO `fa_admin_log` VALUES (48, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"1\",\"type\":\"1\",\"title\":\"asdasd\",\"author\":\"strong\",\"desc\":\"123123\",\"position\":\"123\",\"image\":\"\\/uploads\\/20200829\\/2b5f48b8837334ac48170a8175c19e40.jpg\",\"description\":\"1111\",\"diyname\":\"11\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1598974165);
INSERT INTO `fa_admin_log` VALUES (49, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"pid\":\"15\",\"type\":\"\",\"title\":\"\\u6807\\u9898\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"asdasdsdsdsdsds\",\"diyname\":\"\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599057233);
INSERT INTO `fa_admin_log` VALUES (50, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"type\":\"1\",\"title\":\"\\u6807\\u9898222\",\"author\":\"\\u4f5c\\u8005222\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"2222222\",\"diyname\":\"\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599057394);
INSERT INTO `fa_admin_log` VALUES (51, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"14\",\"type\":\"1\",\"title\":\"\\u6807\\u989811\",\"author\":\"\\u4f5c\\u800511\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"11111\",\"diyname\":\"\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599057674);
INSERT INTO `fa_admin_log` VALUES (52, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"15\",\"type\":\"1\",\"title\":\"\\u65b0\\u95fb\\u51ac\\u5929\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"\\u8bf7\\u95ee\\u8bf7\\u95ee\\u8bf7\\u95ee\\u7684\\u7f3a\\u4e4f\\u8ba4\\u8bc6\\u5bf9\\u65b9\\u6c34\\u7535\\u8d39\",\"diyname\":\"\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599059129);
INSERT INTO `fa_admin_log` VALUES (53, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/6?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"15\",\"type\":\"1\",\"title\":\"\\u65b0\\u95fb\\u51ac\\u5929\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"\\u554a\\u98d2\\u98d2\\u5927\\u8428\\u8fbe\\u901f\\u5ea6\\u554a\\u98d2\\u98d2\\u7684\\u554a\\u98d2\\u98d2\\u5927\\u554a\\u98d2\\u98d2\\u7684\\u5e08\\u7684\",\"diyname\":\"\",\"weigh\":\"6\",\"status\":\"1\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599059150);
INSERT INTO `fa_admin_log` VALUES (54, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/4', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"4\",\"params\":\"ismenu=1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599060408);
INSERT INTO `fa_admin_log` VALUES (55, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599060408);
INSERT INTO `fa_admin_log` VALUES (56, 1, 'admin', '/iVJRvLGpsj.php/addon/state', '插件管理 禁用启用', '{\"name\":\"markdown\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599060443);
INSERT INTO `fa_admin_log` VALUES (57, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599060443);
INSERT INTO `fa_admin_log` VALUES (58, 1, 'admin', '/iVJRvLGpsj.php/addon/install', '插件管理', '{\"name\":\"tinymce\",\"force\":\"0\",\"uid\":\"33720\",\"token\":\"8db9df86-095d-4f31-a834-c62ba7e8f364\",\"version\":\"1.0.5\",\"faversion\":\"1.0.0.20200228_beta\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599061334);
INSERT INTO `fa_admin_log` VALUES (59, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599061335);
INSERT INTO `fa_admin_log` VALUES (60, 1, 'admin', '/iVJRvLGpsj.php/addon/state', '插件管理 禁用启用', '{\"name\":\"markdown\",\"action\":\"disable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599061352);
INSERT INTO `fa_admin_log` VALUES (61, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599061353);
INSERT INTO `fa_admin_log` VALUES (62, 1, 'admin', '/iVJRvLGpsj.php/article/add?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"0\",\"type\":\"\",\"title\":\"\\u6807\\u9898\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"\\u4f60\\u597d\\u6d59\\u6c5f\\u963f\\u65af\\u987f\\u79bb\\u5f00\\u5bb6\\r\\n\\u554a\\u98d2\\u98d2\\u7684\\u963f\\u53cc\\u65b9\\u7684\\u662f\\u7684\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u7535\\u5206\\u516c\\u53f8\\u7684\\u98ce\\u683c\\u662f\\u68b5\\u8482\\u5188\\r\\n\\u554a\\u98d2\\u98d2\\u5927\\u5e08\\u554a\\u98d2\\u98d2\\u7684\\r\\n\\u554a\\u98d2\\u98d2\\u5927\\u5e08\\u5927\\u58f0\\u5927\\u58f0\\u5927\\u58f0\\u9053\\u554a\\u554a\\u98d2\\u98d2\\u7684\\u963f\\u65af\\u987f\\u6cd5\\u56fd\\u7684\\u8eab\\u4efd\\u7684\\u66f4\\u597d\\u53d1\\u6325\\u591a\\u4e2a\\u597d\",\"diyname\":\"\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599061472);
INSERT INTO `fa_admin_log` VALUES (63, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"4401286b0568879b3c021dd93c1d2da2\",\"username\":\"admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599142650);
INSERT INTO `fa_admin_log` VALUES (64, 1, 'admin', '/iVJRvLGpsj.php/category/add?dialog=1', '分类管理 添加', '{\"dialog\":\"1\",\"row\":{\"type\":\"default\",\"pid\":\"0\",\"name\":\"\\u8f6e\\u64ad\\u56fe\",\"nickname\":\"lunbo\",\"image\":\"\",\"keywords\":\"\",\"description\":\"\\u8f6e\\u64ad\\u56fe\\u5206\\u7c7b\",\"weigh\":\"0\",\"status\":\"normal\",\"flag\":[\"\"]}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143105);
INSERT INTO `fa_admin_log` VALUES (65, 1, 'admin', '/iVJRvLGpsj.php/category/edit/ids/14?dialog=1', '分类管理 编辑', '{\"dialog\":\"1\",\"row\":{\"type\":\"article\",\"pid\":\"0\",\"name\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"nickname\":\"suo\",\"image\":\"\",\"keywords\":\"jianjie\",\"description\":\"\\u6240\\u957f\\u7b80\\u4ecb\",\"weigh\":\"14\",\"status\":\"normal\",\"flag\":[\"\"]},\"ids\":\"14\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143150);
INSERT INTO `fa_admin_log` VALUES (66, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/add?dialog=1', '权限管理 菜单规则 添加', '{\"dialog\":\"1\",\"__token__\":\"76d122f85a872f22834aebf3927c82fd\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"rotation\",\"title\":\"\\u8f6e\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa fa-file-photo-o\",\"weigh\":\"0\",\"condition\":\"\",\"remark\":\"\",\"status\":\"normal\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143824);
INSERT INTO `fa_admin_log` VALUES (67, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143825);
INSERT INTO `fa_admin_log` VALUES (68, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"2013-06-13 10.40.01.jpg\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143973);
INSERT INTO `fa_admin_log` VALUES (69, 1, 'admin', '/iVJRvLGpsj.php/rotation/add?dialog=1', '轮播管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"1\",\"name\":\"\\u9e92\\u9e9f\",\"image\":\"\\/uploads\\/20200903\\/f629d7877ba027d70a660fc73055b23b.jpg\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599143993);
INSERT INTO `fa_admin_log` VALUES (70, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,2,6,7,8,3,85,86,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"86\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144092);
INSERT INTO `fa_admin_log` VALUES (71, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144092);
INSERT INTO `fa_admin_log` VALUES (72, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,2,6,7,8,3,85,86,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"86\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144117);
INSERT INTO `fa_admin_log` VALUES (73, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144117);
INSERT INTO `fa_admin_log` VALUES (74, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,2,6,7,8,3,85,86,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"86\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144137);
INSERT INTO `fa_admin_log` VALUES (75, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144137);
INSERT INTO `fa_admin_log` VALUES (76, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,2,6,7,8,3,85,86,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"86\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144320);
INSERT INTO `fa_admin_log` VALUES (77, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144321);
INSERT INTO `fa_admin_log` VALUES (78, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/edit/ids/86?dialog=1', '权限管理 菜单规则 编辑', '{\"dialog\":\"1\",\"__token__\":\"0a25052bea6175aef970c099d5efb9e8\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"rotation\",\"title\":\"\\u8f6e\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa fa-file-photo-o\",\"weigh\":\"2\",\"condition\":\"\",\"remark\":\"\",\"status\":\"normal\"},\"ids\":\"86\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144353);
INSERT INTO `fa_admin_log` VALUES (79, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599144354);
INSERT INTO `fa_admin_log` VALUES (80, 1, 'admin', '/iVJRvLGpsj.php/rotation/edit/ids/2?dialog=1', '轮播管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"17\",\"name\":\"\\u9e92\\u9e9f\",\"image\":\"\\/uploads\\/20200903\\/f629d7877ba027d70a660fc73055b23b.jpg\",\"weigh\":\"2\",\"status\":\"1\"},\"ids\":\"2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599145405);
INSERT INTO `fa_admin_log` VALUES (81, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"baf99aa58b7ceeb7bdb73da08deace41\",\"username\":\"admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599276672);
INSERT INTO `fa_admin_log` VALUES (82, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php%2Farticle%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\\/article?ref=addtabs\",\"__token__\":\"1d5e8eb0a10d5f60f1208998c2e242e4\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599276703);
INSERT INTO `fa_admin_log` VALUES (83, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/4?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"16\",\"type\":\"1\",\"title\":\"\\u5168\\u56fd20\\u591a\\u4e07\\u4eba\\u6b21\\u53c2\\u4e0e \\u56db\\u5ddd\\u7701\\u4eba\\u6c11\",\"author\":\"\\u4f5c\\u8005222\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"2222222\",\"diyname\":\"\",\"weigh\":\"4\",\"status\":\"1\"},\"ids\":\"4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599277647);
INSERT INTO `fa_admin_log` VALUES (84, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/7?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"15\",\"type\":\"1\",\"title\":\"\\u5317\\u4eac\\u536b\\u5065\\u57fa\\u4e1a\\u751f\\u7269\\u7814\\u7a76\\u5173\\u4e8e\\u4e3e\\u529e\\u5168\\u56fd\\u533b\\u7597\\u8d28\\u91cf\\u7ba1\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"\\u4f60\\u597d\\u6d59\\u6c5f\\u963f\\u65af\\u987f\\u79bb\\u5f00\\u5bb6\\r\\n\\u554a\\u98d2\\u98d2\\u7684\\u963f\\u53cc\\u65b9\\u7684\\u662f\\u7684\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u9001\\u5230\\u53d1\\u7535\\u5206\\u516c\\u53f8\\u7684\\u98ce\\u683c\\u662f\\u68b5\\u8482\\u5188\\r\\n\\u554a\\u98d2\\u98d2\\u5927\\u5e08\\u554a\\u98d2\\u98d2\\u7684\\r\\n\\u554a\\u98d2\\u98d2\\u5927\\u5e08\\u5927\\u58f0\\u5927\\u58f0\\u5927\\u58f0\\u9053\\u554a\\u554a\\u98d2\\u98d2\\u7684\\u963f\\u65af\\u987f\\u6cd5\\u56fd\\u7684\\u8eab\\u4efd\\u7684\\u66f4\\u597d\\u53d1\\u6325\\u591a\\u4e2a\\u597d\",\"diyname\":\"\",\"weigh\":\"7\",\"status\":\"1\"},\"ids\":\"7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599277755);
INSERT INTO `fa_admin_log` VALUES (85, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"\\u56fe\\u72471.png\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278064);
INSERT INTO `fa_admin_log` VALUES (86, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/5?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"14\",\"type\":\"1\",\"title\":\"\\u6807\\u989811\",\"author\":\"\\u9ec4\\u6b63\\u660e\",\"desc\":\"\\u533b\\u836f\\u6559\\u80b2\\u5bb6\\uff0c\\u836f\\u7406\\u5b66\\u4e13\\u5bb6\\uff0c \\u535a\\u58eb\\u751f\\u3001\\u7855\\u58eb\\u751f\\u5bfc\\u5e08\\u3002\",\"position\":\"\\u6559\\u6388\",\"image\":\"\\/uploads\\/20200905\\/24c724076aeca1aa07c93e3f6f2e0ed7.png\",\"description\":\"\\u9ec4\\u6b63\\u660e\\uff0c\\u6559\\u6388\\uff0c\\u533b\\u836f\\u6559\\u80b2\\u5bb6\\uff0c\\u836f\\u7406\\u5b66\\u4e13\\u5bb6\\uff0c\\u535a\\u58eb\\u751f\\u3001\\u7855\\u58eb\\u751f\\u5bfc\\u5e08\\u30021992\\u5e74\\u81f32004\\u5e74\\u5de5\\u4f5c\\u4e8e\\u89e3\\u653e\\u519b\\u5317\\u4eac\\u519b\\u533b\\u5b66\\u9662\\u836f\\u7406\\u5b66\\u6559\\u7814\\u5ba4\\uff0c\\u66fe\\u4efb\\u9662\\u9996\\u5e2d\\u4e13\\u5bb6\\u30022004\\u5e74\\u8d77\\u4efb302\\u533b\\u9662\\u836f\\u5b66\\u90e8\\u4e34\\u5e8a\\u836f\\u7406\\u7814\\u7a76\\u5ba4\\u4e3b\\u4efb\\u3002\\u73b0\\u4efb\\u4e2d\\u56fd\\u533b\\u836f\\u6559\\u80b2\\u534f\\u4f1a\\u4f1a\\u957f\\u3001\\u5317\\u4eac\\u536b\\u5065\\u57fa\\u4e1a\\u751f\\u7269\\u6280\\u672f\\u7814\\u7a76\\u6240\\u6240\\u957f\\u3002\\r\\n\\u9ec4\\u6b63\\u660e\\u6559\\u6388\\u4ece\\u4e8b\\u836f\\u7406\\u5b66\\u6559\\u5b66\\u548c\\u79d1\\u5b66\\u7814\\u7a7635\\u5e74\\uff0c\\u4e3b\\u653b\\u65b9\\u5411\\u4e3a\\u4e2d\\u836f\\u836f\\u7406\\u4e0e\\u6297\\u809d\\u708e\\u65b0\\u836f\\u7814\\u53d1\\u3002\\u4ed6\\u5148\\u540e\\u4e3b\\u6301\\u4e86\\u56fd\\u5bb6863\\u3001\\u81ea\\u7136\\u79d1\\u5b66\\u57fa\\u91d1\\u3001\\u56fd\\u5bb6\\u91cd\\u5927\\u4e13\\u9879\\u3001\\u519b\\u961f\\u91cd\\u70b9\\u653b\\u5173\\u7b49\\u8bfe\\u9898\\u5341\\u4f59\\u9879\\uff0c\\u627f\\u62c5\\u6a2a\\u5411\\u8bfe\\u98986\\u9879\\u3002\\u79d1\\u7814\\u671f\\u95f4\\uff0c\\u586b\\u8865\\u4e86\\u56fd\\u5185\\u8fd1\\u4ee3\\u53f2\\u4e0a\\u5bf9\\u6c34\\u82b9\\u7814\\u7a76\\u7684\\u7a7a\\u767d\\uff0c\\u6210\\u529f\\u5f00\\u53d1\\u4e86\\u6c34\\u82b9\\u9897\\u7c92\\u3001\\u82b9\\u9f99\\u9897\\u7c92\\u7b49\\u4e2d\\u836f\\u590d\\u65b9\\u5236\\u5242\\uff0c\\u4ece\\u6c34\\u82b9\\u4e2d\\u63d0\\u53d6\\u51fa\\u6709\\u6548\\u90e8\\u4f4d\\u6c34\\u82b9\\u603b\\u915a\\u9178\\uff0c\\u6210\\u4e3a\\u4e2d\\u56fd\\u8fd1\\u4ee3\\u6c34\\u82b9\\u7814\\u53d1\\u7684\\u7b2c\\u4e00\\u4eba\\u3002\\u5316\\u836f\\u7814\\u7a76\\u65b9\\u9762\\uff0c\\u4ed6\\u7684\\u7814\\u7a76\\u56e2\\u961f\\u53c2\\u4e0e\\u4e86\\u6297\\u4e59\\u809d\\u5316\\u836f1\\u7c7b\\u65b0\\u836f\\u66ff\\u82ac\\u6cf0\\u7684\\u836f\\u7406\\u5b66\\u7814\\u7a76\\uff0c\\u83b7\\u5f97\\u65b0\\u836f\\u4e34\\u5e8a\\u6279\\u4ef6\\u3002\\u66fe\\u83b7\\u5f97\\u519b\\u961f\\u4eba\\u624d\\u57f9\\u517b\\u6700\\u9ad8\\u5956&amp;ldquo;\\u4f2f\\u4e50\\u5956&amp;rdquo;\\u79f0\\u53f7\\uff0c\\u83b7\\u5f97\\u56fd\\u5bb6\\u90e8\\u7ea7\\u548c\\u519b\\u961f\\u79d1\\u5b66\\u8fdb\\u6b65\\u5956\\u4e8c\\u7b49\\u59562\\u9879\\uff0c\\u4e09\\u7b49\\u59567\\u9879\\uff0c\\u7533\\u8bf7\\/\\u6388\\u6743\\u4e13\\u52297\\u9879\\uff0c\\u53d1\\u8868\\u8bba\\u6587\\u767e\\u4f59\\u7bc7\\u3002\\u4e3b\\u7f16\\u300a\\u4e34\\u5e8a\\u836f\\u7406\\u5b66\\u300b\\u3001\\u300a\\u836f\\u7406\\u5b66\\u300b\\u3001\\u300a\\u6297\\u809d\\u708e\\u4e2d\\u836f\\u7684\\u73b0\\u4ee3\\u7814\\u7a76\\u4e0e\\u5e94\\u7528\\u300b\\u7b49\\u6559\\u67509\\u90e8\\uff0c\\u4e3b\\u7f16\\u300a\\u4e2d\\u56fd\\u4e34\\u5e8a\\u836f\\u7269\\u5927\\u8f9e\\u5178\\u300b\\u7b49\\u4e13\\u845710\\u90e8\\uff0c\\u603b\\u7f16\\u533b\\u836f\\u79d1\\u666e\\u300a\\u5408\\u7406\\u7528\\u836f\\u4e00\\u518c\\u901a\\u6653\\u300b\\u7cfb\\u5217\\u4e1b\\u4e6636\\u672c\\u3002\\r\\n\\u9ec4\\u6b63\\u660e\\u6559\\u6388\\u4efb\\u804c\\u5317\\u4eac\\u536b\\u5065\\u57fa\\u4e1a\\u751f\\u7269\\u6280\\u672f\\u7814\\u7a76\\u6240\\u6240\\u957f\\u4ee5\\u6765\\uff0c\\u5148\\u540e\\u627f\\u63a5\\u4e86\\u4e09\\u7c7b\\u533b\\u7597\\u5668\\u68b0\\u9aa8\\u9ecf\\u5408\\u5242\\u7684\\u7814\\u7a76\\u3001\\u6297\\u4e59\\u809d\\u4e2d\\u836fAH40\\u7684\\u836f\\u7406\\u5b66\\u7814\\u7a76\\u3001\\u725b\\u6a1f\\u829d\\u6db2\\u5bf9\\u70eb\\u4f24\\u52a8\\u7269\\u6a21\\u578b\\u4f5c\\u7528\\u7684\\u8bd5\\u9a8c\\u7814\\u7a76\\u3001\\u6c34\\u82b9\\u603b\\u915a\\u9178\\u6297HIV\\u836f\\u6548\\u5b66\\u4f5c\\u7528\\u53ca\\u673a\\u5236\\u7684\\u7814\\u7a76\\u7b49\\u79d1\\u7814\\u9879\\u76ee\\uff0c\\u5e76\\u4e0e\\u591a\\u6240\\u9ad8\\u6821\\u3001\\u79d1\\u7814\\u673a\\u6784\\u4ee5\\u53ca\\u533b\\u836f\\u4f01\\u4e1a\\u5efa\\u7acb\\u4e86\\u826f\\u597d\\u7684\\u5408\\u4f5c\\u5173\\u7cfb\\uff0c\\u4e3a\\u7814\\u7a76\\u6240\\u7684\\u53d1\\u5c55\\u6253\\u4e0b\\u4e86\\u575a\\u5b9e\\u7684\\u57fa\\u7840\\u3002\",\"diyname\":\"\",\"weigh\":\"5\",\"status\":\"1\"},\"ids\":\"5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278072);
INSERT INTO `fa_admin_log` VALUES (87, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/3?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"16\",\"type\":\"\",\"title\":\"\\u65b0\\u578b\\u51a0\\u72b6\\u80ba\\u708e\\u79d1\\u5b66\\u9632\\u6b62\\u7ebf\\u4e0a\\u7814\\u8ba8\\u4f1a\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\",\"position\":\"\",\"image\":\"\",\"description\":\"asdasdsdsdsdsds\",\"diyname\":\"\",\"weigh\":\"3\",\"status\":\"1\"},\"ids\":\"3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278101);
INSERT INTO `fa_admin_log` VALUES (88, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/2?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"16\",\"type\":\"1\",\"title\":\"2020\\u5e74\\u65b0\\u6625\\u56e2\\u62dc\\u4f1a\\u5728\\u5317\\u4eac\\u53ec\\u5f00\",\"author\":\"strong\",\"desc\":\"123123\",\"position\":\"123\",\"image\":\"\\/uploads\\/20200829\\/2b5f48b8837334ac48170a8175c19e40.jpg\",\"description\":\"1111\",\"diyname\":\"11\",\"weigh\":\"2\",\"status\":\"1\"},\"ids\":\"2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278119);
INSERT INTO `fa_admin_log` VALUES (89, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/1?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"15\",\"type\":\"1\",\"title\":\"\\u4e2d\\u56fd\\u98df\\u54c1\\u5b89\\u5168\\u6559\\u80b2\\u9ad8\\u5cf0\\u8bba\\u575b\\u5728\\u6c49\\u4e3e\\u884c\\uff0c\\u4e13\\u5bb6\\u5171\\u8bdd\\u4e0e\\u2026\",\"author\":\"\\u4f5c\\u8005\",\"desc\":\"\\u963f\\u8428\\u5fb7\",\"position\":\"12312\\u53bb\",\"image\":\"\\/uploads\\/20200829\\/cc64fc0acc5310e998734a8e75a6b374.png\",\"description\":\"1231312\",\"diyname\":\"3123123123\",\"weigh\":\"1\",\"status\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278140);
INSERT INTO `fa_admin_log` VALUES (90, 1, 'admin', '/iVJRvLGpsj.php/article/edit/ids/5?dialog=1', '文章管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"14\",\"type\":\"1\",\"title\":\"\\u9ec4\\u6b63\\u660e\",\"author\":\"\\u9ec4\\u6b63\\u660e\",\"desc\":\"\\u533b\\u836f\\u6559\\u80b2\\u5bb6\\uff0c\\u836f\\u7406\\u5b66\\u4e13\\u5bb6\\uff0c \\u535a\\u58eb\\u751f\\u3001\\u7855\\u58eb\\u751f\\u5bfc\\u5e08\\u3002\",\"position\":\"\\u6559\\u6388\",\"image\":\"\\/uploads\\/20200905\\/24c724076aeca1aa07c93e3f6f2e0ed7.png\",\"description\":\"\\u9ec4\\u6b63\\u660e\\uff0c\\u6559\\u6388\\uff0c\\u533b\\u836f\\u6559\\u80b2\\u5bb6\\uff0c\\u836f\\u7406\\u5b66\\u4e13\\u5bb6\\uff0c\\u535a\\u58eb\\u751f\\u3001\\u7855\\u58eb\\u751f\\u5bfc\\u5e08\\u30021992\\u5e74\\u81f32004\\u5e74\\u5de5\\u4f5c\\u4e8e\\u89e3\\u653e\\u519b\\u5317\\u4eac\\u519b\\u533b\\u5b66\\u9662\\u836f\\u7406\\u5b66\\u6559\\u7814\\u5ba4\\uff0c\\u66fe\\u4efb\\u9662\\u9996\\u5e2d\\u4e13\\u5bb6\\u30022004\\u5e74\\u8d77\\u4efb302\\u533b\\u9662\\u836f\\u5b66\\u90e8\\u4e34\\u5e8a\\u836f\\u7406\\u7814\\u7a76\\u5ba4\\u4e3b\\u4efb\\u3002\\u73b0\\u4efb\\u4e2d\\u56fd\\u533b\\u836f\\u6559\\u80b2\\u534f\\u4f1a\\u4f1a\\u957f\\u3001\\u5317\\u4eac\\u536b\\u5065\\u57fa\\u4e1a\\u751f\\u7269\\u6280\\u672f\\u7814\\u7a76\\u6240\\u6240\\u957f\\u3002\\r\\n\\u9ec4\\u6b63\\u660e\\u6559\\u6388\\u4ece\\u4e8b\\u836f\\u7406\\u5b66\\u6559\\u5b66\\u548c\\u79d1\\u5b66\\u7814\\u7a7635\\u5e74\\uff0c\\u4e3b\\u653b\\u65b9\\u5411\\u4e3a\\u4e2d\\u836f\\u836f\\u7406\\u4e0e\\u6297\\u809d\\u708e\\u65b0\\u836f\\u7814\\u53d1\\u3002\\u4ed6\\u5148\\u540e\\u4e3b\\u6301\\u4e86\\u56fd\\u5bb6863\\u3001\\u81ea\\u7136\\u79d1\\u5b66\\u57fa\\u91d1\\u3001\\u56fd\\u5bb6\\u91cd\\u5927\\u4e13\\u9879\\u3001\\u519b\\u961f\\u91cd\\u70b9\\u653b\\u5173\\u7b49\\u8bfe\\u9898\\u5341\\u4f59\\u9879\\uff0c\\u627f\\u62c5\\u6a2a\\u5411\\u8bfe\\u98986\\u9879\\u3002\\u79d1\\u7814\\u671f\\u95f4\\uff0c\\u586b\\u8865\\u4e86\\u56fd\\u5185\\u8fd1\\u4ee3\\u53f2\\u4e0a\\u5bf9\\u6c34\\u82b9\\u7814\\u7a76\\u7684\\u7a7a\\u767d\\uff0c\\u6210\\u529f\\u5f00\\u53d1\\u4e86\\u6c34\\u82b9\\u9897\\u7c92\\u3001\\u82b9\\u9f99\\u9897\\u7c92\\u7b49\\u4e2d\\u836f\\u590d\\u65b9\\u5236\\u5242\\uff0c\\u4ece\\u6c34\\u82b9\\u4e2d\\u63d0\\u53d6\\u51fa\\u6709\\u6548\\u90e8\\u4f4d\\u6c34\\u82b9\\u603b\\u915a\\u9178\\uff0c\\u6210\\u4e3a\\u4e2d\\u56fd\\u8fd1\\u4ee3\\u6c34\\u82b9\\u7814\\u53d1\\u7684\\u7b2c\\u4e00\\u4eba\\u3002\\u5316\\u836f\\u7814\\u7a76\\u65b9\\u9762\\uff0c\\u4ed6\\u7684\\u7814\\u7a76\\u56e2\\u961f\\u53c2\\u4e0e\\u4e86\\u6297\\u4e59\\u809d\\u5316\\u836f1\\u7c7b\\u65b0\\u836f\\u66ff\\u82ac\\u6cf0\\u7684\\u836f\\u7406\\u5b66\\u7814\\u7a76\\uff0c\\u83b7\\u5f97\\u65b0\\u836f\\u4e34\\u5e8a\\u6279\\u4ef6\\u3002\\u66fe\\u83b7\\u5f97\\u519b\\u961f\\u4eba\\u624d\\u57f9\\u517b\\u6700\\u9ad8\\u5956&amp;ldquo;\\u4f2f\\u4e50\\u5956&amp;rdquo;\\u79f0\\u53f7\\uff0c\\u83b7\\u5f97\\u56fd\\u5bb6\\u90e8\\u7ea7\\u548c\\u519b\\u961f\\u79d1\\u5b66\\u8fdb\\u6b65\\u5956\\u4e8c\\u7b49\\u59562\\u9879\\uff0c\\u4e09\\u7b49\\u59567\\u9879\\uff0c\\u7533\\u8bf7\\/\\u6388\\u6743\\u4e13\\u52297\\u9879\\uff0c\\u53d1\\u8868\\u8bba\\u6587\\u767e\\u4f59\\u7bc7\\u3002\\u4e3b\\u7f16\\u300a\\u4e34\\u5e8a\\u836f\\u7406\\u5b66\\u300b\\u3001\\u300a\\u836f\\u7406\\u5b66\\u300b\\u3001\\u300a\\u6297\\u809d\\u708e\\u4e2d\\u836f\\u7684\\u73b0\\u4ee3\\u7814\\u7a76\\u4e0e\\u5e94\\u7528\\u300b\\u7b49\\u6559\\u67509\\u90e8\\uff0c\\u4e3b\\u7f16\\u300a\\u4e2d\\u56fd\\u4e34\\u5e8a\\u836f\\u7269\\u5927\\u8f9e\\u5178\\u300b\\u7b49\\u4e13\\u845710\\u90e8\\uff0c\\u603b\\u7f16\\u533b\\u836f\\u79d1\\u666e\\u300a\\u5408\\u7406\\u7528\\u836f\\u4e00\\u518c\\u901a\\u6653\\u300b\\u7cfb\\u5217\\u4e1b\\u4e6636\\u672c\\u3002\\r\\n\\u9ec4\\u6b63\\u660e\\u6559\\u6388\\u4efb\\u804c\\u5317\\u4eac\\u536b\\u5065\\u57fa\\u4e1a\\u751f\\u7269\\u6280\\u672f\\u7814\\u7a76\\u6240\\u6240\\u957f\\u4ee5\\u6765\\uff0c\\u5148\\u540e\\u627f\\u63a5\\u4e86\\u4e09\\u7c7b\\u533b\\u7597\\u5668\\u68b0\\u9aa8\\u9ecf\\u5408\\u5242\\u7684\\u7814\\u7a76\\u3001\\u6297\\u4e59\\u809d\\u4e2d\\u836fAH40\\u7684\\u836f\\u7406\\u5b66\\u7814\\u7a76\\u3001\\u725b\\u6a1f\\u829d\\u6db2\\u5bf9\\u70eb\\u4f24\\u52a8\\u7269\\u6a21\\u578b\\u4f5c\\u7528\\u7684\\u8bd5\\u9a8c\\u7814\\u7a76\\u3001\\u6c34\\u82b9\\u603b\\u915a\\u9178\\u6297HIV\\u836f\\u6548\\u5b66\\u4f5c\\u7528\\u53ca\\u673a\\u5236\\u7684\\u7814\\u7a76\\u7b49\\u79d1\\u7814\\u9879\\u76ee\\uff0c\\u5e76\\u4e0e\\u591a\\u6240\\u9ad8\\u6821\\u3001\\u79d1\\u7814\\u673a\\u6784\\u4ee5\\u53ca\\u533b\\u836f\\u4f01\\u4e1a\\u5efa\\u7acb\\u4e86\\u826f\\u597d\\u7684\\u5408\\u4f5c\\u5173\\u7cfb\\uff0c\\u4e3a\\u7814\\u7a76\\u6240\\u7684\\u53d1\\u5c55\\u6253\\u4e0b\\u4e86\\u575a\\u5b9e\\u7684\\u57fa\\u7840\\u3002\",\"diyname\":\"\",\"weigh\":\"5\",\"status\":\"1\"},\"ids\":\"5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599278208);
INSERT INTO `fa_admin_log` VALUES (91, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/6', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"6\",\"params\":\"ismenu=0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599311369);
INSERT INTO `fa_admin_log` VALUES (92, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599311370);
INSERT INTO `fa_admin_log` VALUES (93, 1, 'admin', '/iVJRvLGpsj.php/ajax/weigh', '', '{\"ids\":\"1,6,7,8,3,86,85,2,4,5,9,10,11,12,66,67,73,79\",\"changeid\":\"2\",\"pid\":\"0\",\"field\":\"weigh\",\"orderway\":\"desc\",\"table\":\"auth_rule\",\"pk\":\"id\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599311386);
INSERT INTO `fa_admin_log` VALUES (94, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599311386);
INSERT INTO `fa_admin_log` VALUES (95, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/multi/ids/4', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"4\",\"params\":\"ismenu=0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599398533);
INSERT INTO `fa_admin_log` VALUES (96, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36', 1599398533);
INSERT INTO `fa_admin_log` VALUES (97, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"93ac185cddcf7cfc1e522ac11930c93c\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599533756);
INSERT INTO `fa_admin_log` VALUES (98, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"2036042698b34a2c67849fe0875c94e3\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 1599533812);
INSERT INTO `fa_admin_log` VALUES (99, 1, 'admin', '/iVJRvLGpsj.php/index/login?url=%2FiVJRvLGpsj.php', '登录', '{\"url\":\"\\/iVJRvLGpsj.php\",\"__token__\":\"29219999401ad3fff7f21913d81817fc\",\"username\":\"admin\",\"keeplogin\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599537226);
INSERT INTO `fa_admin_log` VALUES (100, 1, 'admin', '/iVJRvLGpsj.php/auth/rule/edit/ids/86?dialog=1', '权限管理 菜单规则 编辑', '{\"dialog\":\"1\",\"__token__\":\"fcd2436e04de6d405ec44d49cd8939c7\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"rotation\",\"title\":\"\\u8f6e\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa fa-file-photo-o\",\"weigh\":\"99\",\"condition\":\"\",\"remark\":\"\",\"status\":\"normal\"},\"ids\":\"86\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599537337);
INSERT INTO `fa_admin_log` VALUES (101, 1, 'admin', '/iVJRvLGpsj.php/index/index', '', '{\"action\":\"refreshmenu\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599537337);
INSERT INTO `fa_admin_log` VALUES (102, 1, 'admin', '/iVJRvLGpsj.php/rotation/edit/ids/1?dialog=1', '轮播管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"17\",\"name\":\"\\u8f6e\\u64ad\",\"image\":\"\\/uploads\\/20200829\\/3ab04b7b18bc70c20bde7e72d51be817.jpg\",\"weigh\":\"0\",\"status\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599537375);
INSERT INTO `fa_admin_log` VALUES (103, 1, 'admin', '/iVJRvLGpsj.php/rotation/add?dialog=1', '轮播管理', '{\"dialog\":\"1\",\"row\":{\"cid\":\"16\",\"name\":\"tutu\",\"image\":\"\\/uploads\\/20200829\\/2b5f48b8837334ac48170a8175c19e40.jpg\",\"weigh\":\"0\",\"status\":\"1\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1599537435);
INSERT INTO `fa_admin_log` VALUES (104, 1, 'admin', '/iVJRvLGpsj.php/ajax/upload', '', '{\"name\":\"1e30e924b899a901d39c1e3813950a7b0208f517.jpeg\"}', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 1599553753);

-- ----------------------------
-- Table structure for fa_area
-- ----------------------------
DROP TABLE IF EXISTS `fa_area`;
CREATE TABLE `fa_area`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) NULL DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '简称',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) NULL DEFAULT NULL COMMENT '层级 0 1 2 省市区县',
  `pinyin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_area
-- ----------------------------

-- ----------------------------
-- Table structure for fa_article
-- ----------------------------
DROP TABLE IF EXISTS `fa_article`;
CREATE TABLE `fa_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '栏目类型',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '标题',
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '作者',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '简介',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '职位',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `diyname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重排序',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `weigh`(`weigh`, `id`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_article
-- ----------------------------
INSERT INTO `fa_article` VALUES (1, 15, '1', '中国食品安全教育高峰论坛在汉举行，专家共话与…', '作者', '阿萨德', '12312去', '/uploads/20200829/cc64fc0acc5310e998734a8e75a6b374.png', '<p>1231312</p>', '3123123123', NULL, 1599278140, 1, '1');
INSERT INTO `fa_article` VALUES (2, 16, '1', '2020年新春团拜会在北京召开', 'strong', '123123', '123', '/uploads/20200829/2b5f48b8837334ac48170a8175c19e40.jpg', '<p>1111</p>', '11', 1598974165, 1599278119, 2, '1');
INSERT INTO `fa_article` VALUES (3, 16, '', '新型冠状肺炎科学防止线上研讨会', '作者', '', '', '', '<p>asdasdsdsdsdsds</p>', '', 1599057232, 1599278101, 3, '1');
INSERT INTO `fa_article` VALUES (4, 16, '1', '全国20多万人次参与 四川省人民', '作者222', '', '', '', '<p>2222222</p>', '', 1599057394, 1599277647, 4, '1');
INSERT INTO `fa_article` VALUES (5, 14, '1', '黄正明', '黄正明', '医药教育家，药理学专家， 博士生、硕士生导师。', '教授', '/uploads/20200905/24c724076aeca1aa07c93e3f6f2e0ed7.png', '<p style=\"text-align: left;\">黄正明，教授，医药教育家，药理学专家，博士生、硕士生导师。1992年至2004年工作于解放军北京军医学院药理学教研室，曾任院首席专家。2004年起任302医院药学部临床药理研究室主任。现任中国医药教育协会会长、北京卫健基业生物技术研究所所长。</p>\r\n<p style=\"text-align: left;\">黄正明教授从事药理学教学和科学研究35年，主攻方向为中药药理与抗肝炎新药研发。他先后主持了国家863、自然科学基金、国家重大专项、军队重点攻关等课题十余项，承担横向课题6项。科研期间，填补了国内近代史上对水芹研究的空白，成功开发了水芹颗粒、芹龙颗粒等中药复方制剂，从水芹中提取出有效部位水芹总酚酸，成为中国近代水芹研发的第一人。化药研究方面，他的研究团队参与了抗乙肝化药1类新药替芬泰的药理学研究，获得新药临床批件。曾获得军队人才培养最高奖&ldquo;伯乐奖&rdquo;称号，获得国家部级和军队科学进步奖二等奖2项，三等奖7项，申请/授权专利7项，发表论文百余篇。主编《临床药理学》、《药理学》、《抗肝炎中药的现代研究与应用》等教材9部，主编《中国临床药物大辞典》等专著10部，总编医药科普《合理用药一册通晓》系列丛书36本。</p>\r\n<p style=\"text-align: left;\">黄正明教授任职北京卫健基业生物技术研究所所长以来，先后承接了三类医疗器械骨黏合剂的研究、抗乙肝中药AH40的药理学研究、牛樟芝液对烫伤动物模型作用的试验研究、水芹总酚酸抗HIV药效学作用及机制的研究等科研项目，并与多所高校、科研机构以及医药企业建立了良好的合作关系，为研究所的发展打下了坚实的基础。</p>', '', 1599057674, 1599278208, 5, '1');
INSERT INTO `fa_article` VALUES (6, 15, '1', '新闻冬天', '作者', '', '', '', '啊飒飒大萨达速度啊飒飒的啊飒飒大啊飒飒的师的', '', 1599059129, 1599059150, 6, '1');
INSERT INTO `fa_article` VALUES (7, 15, '1', '北京卫健基业生物研究关于举办全国医疗质量管', '作者', '', '', '', '<p><strong>你好浙江阿斯顿离开家</strong></p>\r\n<p>啊飒飒的阿双方的是的发送到发送到发送到发送到发电分公司的风格是梵蒂冈</p>\r\n<p>啊飒飒大师啊飒飒的</p>\r\n<p>啊飒飒大师大声大声大声道啊啊飒飒的阿斯顿法国的身份的更好发挥多个好</p>', '', 1599061471, 1599277754, 7, '1 ');

-- ----------------------------
-- Table structure for fa_attachment
-- ----------------------------
DROP TABLE IF EXISTS `fa_attachment`;
CREATE TABLE `fa_attachment`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片帧数',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `mimetype` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) NULL DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_attachment
-- ----------------------------
INSERT INTO `fa_attachment` VALUES (1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 21859, 'image/png', '', 1499681848, 1499681848, 1499681848, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6');
INSERT INTO `fa_attachment` VALUES (2, 1, 0, '/uploads/20200829/cc64fc0acc5310e998734a8e75a6b374.png', '870', '848', 'png', 0, 34424, 'image/png', '{\"name\":\"logo1.png\"}', 1598675004, 1598675004, 1598675004, 'local', 'c09f887a564d18e061d66953eba3847358cf1803');
INSERT INTO `fa_attachment` VALUES (3, 1, 0, '/uploads/20200829/3ab04b7b18bc70c20bde7e72d51be817.jpg', '624', '464', 'jpg', 0, 69450, 'image/jpeg', '{\"name\":\"\\u9996\\u9875\\u52a8\\u56fe.jpg\"}', 1598676139, 1598676139, 1598676139, 'local', '40794d6cae5387821bb40921e4cdcc3b03c245d5');
INSERT INTO `fa_attachment` VALUES (4, 1, 0, '/uploads/20200829/2b5f48b8837334ac48170a8175c19e40.jpg', '1280', '2496', 'jpg', 0, 642348, 'image/jpeg', '{\"name\":\"\\u9996\\u9875\\u56fe.jpg\"}', 1598685312, 1598685312, 1598685312, 'local', '516e87b0ea0edcebc288947b026ea00467382680');
INSERT INTO `fa_attachment` VALUES (5, 1, 0, '/uploads/20200903/f629d7877ba027d70a660fc73055b23b.jpg', '2560', '1920', 'jpg', 0, 1367380, 'image/jpeg', '{\"name\":\"2013-06-13 10.40.01.jpg\"}', 1599143973, 1599143973, 1599143973, 'local', 'db8f2f197e4366fd1c7cd9348cc7ff9166b98502');
INSERT INTO `fa_attachment` VALUES (6, 1, 0, '/uploads/20200905/24c724076aeca1aa07c93e3f6f2e0ed7.png', '293', '403', 'png', 0, 103903, 'image/png', '{\"name\":\"\\u56fe\\u72471.png\"}', 1599278064, 1599278064, 1599278064, 'local', '7e4a51fcafe67598ee2cb68213435d18a68dcf03');
INSERT INTO `fa_attachment` VALUES (7, 1, 0, '/uploads/20200908/f8ea59d06aa03631571ba00cb55f22b7.jpeg', '500', '333', 'jpeg', 0, 41128, 'image/jpeg', '{\"name\":\"1e30e924b899a901d39c1e3813950a7b0208f517.jpeg\"}', 1599553753, 1599553753, 1599553753, 'local', '25f10427498dbdb693cf79200d44bd0b4546d60d');

-- ----------------------------
-- Table structure for fa_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_group`;
CREATE TABLE `fa_auth_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父组别',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_group
-- ----------------------------
INSERT INTO `fa_auth_group` VALUES (1, 0, 'Admin group', '*', 1490883540, 149088354, 'normal');
INSERT INTO `fa_auth_group` VALUES (2, 1, 'Second group', '13,14,16,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,1,9,10,11,7,6,8,2,4,5', 1490883540, 1505465692, 'normal');
INSERT INTO `fa_auth_group` VALUES (3, 2, 'Third group', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,5', 1490883540, 1502205322, 'normal');
INSERT INTO `fa_auth_group` VALUES (4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1490883540, 1502205350, 'normal');
INSERT INTO `fa_auth_group` VALUES (5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1490883540, 1502205344, 'normal');

-- ----------------------------
-- Table structure for fa_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_group_access`;
CREATE TABLE `fa_auth_group_access`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID',
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_group_access
-- ----------------------------
INSERT INTO `fa_auth_group_access` VALUES (1, 1);

-- ----------------------------
-- Table structure for fa_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_rule`;
CREATE TABLE `fa_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为菜单',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `weigh`(`weigh`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_rule
-- ----------------------------
INSERT INTO `fa_auth_rule` VALUES (1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', 'Dashboard tips', 0, 1497429920, 1598674900, 137, 'normal');
INSERT INTO `fa_auth_rule` VALUES (2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', 1, 1497429920, 1497430169, 1, 'normal');
INSERT INTO `fa_auth_rule` VALUES (3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', 'Category tips', 1, 1497429920, 1497429920, 119, 'normal');
INSERT INTO `fa_auth_rule` VALUES (4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', 'Addon tips', 0, 1502035509, 1599398533, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', 1, 1497429920, 1497430092, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', 'Config tips', 0, 1497429920, 1599311369, 60, 'normal');
INSERT INTO `fa_auth_rule` VALUES (7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', 'Attachment tips', 1, 1497429920, 1497430699, 53, 'normal');
INSERT INTO `fa_auth_rule` VALUES (8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', 0, 1497429920, 1598676427, 34, 'normal');
INSERT INTO `fa_auth_rule` VALUES (9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', 'Admin tips', 1, 1497429920, 1497430320, 118, 'normal');
INSERT INTO `fa_auth_rule` VALUES (10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', 'Admin log tips', 1, 1497429920, 1497430307, 113, 'normal');
INSERT INTO `fa_auth_rule` VALUES (11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', 'Group tips', 1, 1497429920, 1497429920, 109, 'normal');
INSERT INTO `fa_auth_rule` VALUES (12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', 'Rule tips', 1, 1497429920, 1497430581, 104, 'normal');
INSERT INTO `fa_auth_rule` VALUES (13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 136, 'normal');
INSERT INTO `fa_auth_rule` VALUES (14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 135, 'normal');
INSERT INTO `fa_auth_rule` VALUES (15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 133, 'normal');
INSERT INTO `fa_auth_rule` VALUES (16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 134, 'normal');
INSERT INTO `fa_auth_rule` VALUES (17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 132, 'normal');
INSERT INTO `fa_auth_rule` VALUES (18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 52, 'normal');
INSERT INTO `fa_auth_rule` VALUES (19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 51, 'normal');
INSERT INTO `fa_auth_rule` VALUES (20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 50, 'normal');
INSERT INTO `fa_auth_rule` VALUES (21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 49, 'normal');
INSERT INTO `fa_auth_rule` VALUES (22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 48, 'normal');
INSERT INTO `fa_auth_rule` VALUES (23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', 'Attachment tips', 0, 1497429920, 1497429920, 59, 'normal');
INSERT INTO `fa_auth_rule` VALUES (24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 58, 'normal');
INSERT INTO `fa_auth_rule` VALUES (25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 57, 'normal');
INSERT INTO `fa_auth_rule` VALUES (26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 56, 'normal');
INSERT INTO `fa_auth_rule` VALUES (27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 55, 'normal');
INSERT INTO `fa_auth_rule` VALUES (28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 54, 'normal');
INSERT INTO `fa_auth_rule` VALUES (29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 33, 'normal');
INSERT INTO `fa_auth_rule` VALUES (30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 32, 'normal');
INSERT INTO `fa_auth_rule` VALUES (31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 31, 'normal');
INSERT INTO `fa_auth_rule` VALUES (32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 30, 'normal');
INSERT INTO `fa_auth_rule` VALUES (33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 29, 'normal');
INSERT INTO `fa_auth_rule` VALUES (34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 28, 'normal');
INSERT INTO `fa_auth_rule` VALUES (35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', 'Category tips', 0, 1497429920, 1497429920, 142, 'normal');
INSERT INTO `fa_auth_rule` VALUES (36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 141, 'normal');
INSERT INTO `fa_auth_rule` VALUES (37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 140, 'normal');
INSERT INTO `fa_auth_rule` VALUES (38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 139, 'normal');
INSERT INTO `fa_auth_rule` VALUES (39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 138, 'normal');
INSERT INTO `fa_auth_rule` VALUES (40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', 'Admin tips', 0, 1497429920, 1497429920, 117, 'normal');
INSERT INTO `fa_auth_rule` VALUES (41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 116, 'normal');
INSERT INTO `fa_auth_rule` VALUES (42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 115, 'normal');
INSERT INTO `fa_auth_rule` VALUES (43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 114, 'normal');
INSERT INTO `fa_auth_rule` VALUES (44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', 'Admin log tips', 0, 1497429920, 1497429920, 112, 'normal');
INSERT INTO `fa_auth_rule` VALUES (45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 111, 'normal');
INSERT INTO `fa_auth_rule` VALUES (46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 110, 'normal');
INSERT INTO `fa_auth_rule` VALUES (47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', 'Group tips', 0, 1497429920, 1497429920, 108, 'normal');
INSERT INTO `fa_auth_rule` VALUES (48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 107, 'normal');
INSERT INTO `fa_auth_rule` VALUES (49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 106, 'normal');
INSERT INTO `fa_auth_rule` VALUES (50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 105, 'normal');
INSERT INTO `fa_auth_rule` VALUES (51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', 'Rule tips', 0, 1497429920, 1497429920, 103, 'normal');
INSERT INTO `fa_auth_rule` VALUES (52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 102, 'normal');
INSERT INTO `fa_auth_rule` VALUES (53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 101, 'normal');
INSERT INTO `fa_auth_rule` VALUES (54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 100, 'normal');
INSERT INTO `fa_auth_rule` VALUES (55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', 'Addon tips', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', 1, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', 1, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', 1, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', 1, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (85, 'file', 0, 'article', '文章管理', 'fa fa-paste', '', '', 1, 1598752914, 1598752977, 2, 'normal');
INSERT INTO `fa_auth_rule` VALUES (86, 'file', 0, 'rotation', '轮播管理', 'fa fa-file-photo-o', '', '', 1, 1599143824, 1599537337, 99, 'normal');

-- ----------------------------
-- Table structure for fa_category
-- ----------------------------
DROP TABLE IF EXISTS `fa_category`;
CREATE TABLE `fa_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `flag` set('hot','index','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `weigh`(`weigh`, `id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_category
-- ----------------------------
INSERT INTO `fa_category` VALUES (14, 0, 'article', '所长简介', 'suo', '', '', 'jianjie', '所长简介', '', 1598677028, 1599143150, 14, 'normal');
INSERT INTO `fa_category` VALUES (15, 0, 'article', '新闻动态', 'news', '', '', 'news', 'news', '', 1598677137, 1598677137, 15, 'normal');
INSERT INTO `fa_category` VALUES (16, 0, 'article', '科研动态', 'keyan', '', '/uploads/20200829/2b5f48b8837334ac48170a8175c19e40.jpg', 'keyan', 'keyan', '', 1598677168, 1598685316, 16, 'normal');
INSERT INTO `fa_category` VALUES (17, 0, 'default', '轮播图', 'lunbo', '', '', '', '轮播图分类', '', 1599143104, 1599143104, 17, 'normal');

-- ----------------------------
-- Table structure for fa_config
-- ----------------------------
DROP TABLE IF EXISTS `fa_config`;
CREATE TABLE `fa_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量值',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量字典数据',
  `rule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '扩展属性',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_config
-- ----------------------------
INSERT INTO `fa_config` VALUES (1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', '后台管理', '', 'required', '');
INSERT INTO `fa_config` VALUES (2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '', '', '', '');
INSERT INTO `fa_config` VALUES (3, 'cdnurl', 'basic', 'Cdn url', '如果静态资源使用第三方云储存请配置该值', 'string', '', '', '', '');
INSERT INTO `fa_config` VALUES (4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '1.0.1', '', 'required', '');
INSERT INTO `fa_config` VALUES (5, 'timezone', 'basic', 'Timezone', '', 'string', 'Asia/Shanghai', '', 'required', '');
INSERT INTO `fa_config` VALUES (6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', '');
INSERT INTO `fa_config` VALUES (7, 'languages', 'basic', 'Languages', '', 'array', '{\"backend\":\"zh-cn\",\"frontend\":\"zh-cn\"}', '', 'required', '');
INSERT INTO `fa_config` VALUES (8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', '');
INSERT INTO `fa_config` VALUES (9, 'categorytype', 'dictionary', 'Category type', '', 'array', '{\"default\":\"Default\",\"page\":\"Page\",\"article\":\"Article\",\"test\":\"Test\"}', '', '', '');
INSERT INTO `fa_config` VALUES (10, 'configgroup', 'dictionary', 'Config group', '', 'array', '{\"basic\":\"Basic\",\"email\":\"Email\",\"dictionary\":\"Dictionary\",\"user\":\"User\",\"example\":\"Example\"}', '', '', '');
INSERT INTO `fa_config` VALUES (11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '1', '[\"Please select\",\"SMTP\",\"Mail\"]', '', '');
INSERT INTO `fa_config` VALUES (12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', 'smtp.qq.com', '', '', '');
INSERT INTO `fa_config` VALUES (13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '465', '', '', '');
INSERT INTO `fa_config` VALUES (14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', '10000', '', '', '');
INSERT INTO `fa_config` VALUES (15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码）', 'string', 'password', '', '', '');
INSERT INTO `fa_config` VALUES (16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '2', '[\"None\",\"TLS\",\"SSL\"]', '', '');
INSERT INTO `fa_config` VALUES (17, 'mail_from', 'email', 'Mail from', '', 'string', '10000@qq.com', '', '', '');
INSERT INTO `fa_config` VALUES (18, 'gongsi', 'email', 'gongsi', '', 'text', '北京医药教育协会', '', '', '');

-- ----------------------------
-- Table structure for fa_ems
-- ----------------------------
DROP TABLE IF EXISTS `fa_ems`;
CREATE TABLE `fa_ems`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '事件',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮箱验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_ems
-- ----------------------------

-- ----------------------------
-- Table structure for fa_endpage
-- ----------------------------
DROP TABLE IF EXISTS `fa_endpage`;
CREATE TABLE `fa_endpage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dizhi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dianhua` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `youbian` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `youxiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_endpage
-- ----------------------------

-- ----------------------------
-- Table structure for fa_images
-- ----------------------------
DROP TABLE IF EXISTS `fa_images`;
CREATE TABLE `fa_images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '1轮播 2新闻',
  `status` int(11) NULL DEFAULT NULL,
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_images
-- ----------------------------

-- ----------------------------
-- Table structure for fa_rotation
-- ----------------------------
DROP TABLE IF EXISTS `fa_rotation`;
CREATE TABLE `fa_rotation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_rotation
-- ----------------------------
INSERT INTO `fa_rotation` VALUES (1, 17, '轮播', '/uploads/20200829/3ab04b7b18bc70c20bde7e72d51be817.jpg', NULL, 1599537375, 0, '1');
INSERT INTO `fa_rotation` VALUES (2, 17, '麒麟', '/uploads/20200903/f629d7877ba027d70a660fc73055b23b.jpg', 1599143993, 1599145405, 2, '1');
INSERT INTO `fa_rotation` VALUES (3, 16, 'tutu', '/uploads/20200829/2b5f48b8837334ac48170a8175c19e40.jpg', 1599537435, 1599537435, 3, '1');

-- ----------------------------
-- Table structure for fa_sms
-- ----------------------------
DROP TABLE IF EXISTS `fa_sms`;
CREATE TABLE `fa_sms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '短信验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_sms
-- ----------------------------

-- ----------------------------
-- Table structure for fa_test
-- ----------------------------
DROP TABLE IF EXISTS `fa_test`;
CREATE TABLE `fa_test`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID(单选)',
  `category_ids` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `images` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '省市',
  `json` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `price` float(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '价格',
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击',
  `startdate` date NULL DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime(0) NULL DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year NULL DEFAULT NULL COMMENT '年',
  `times` time(0) NULL DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) NULL DEFAULT NULL COMMENT '刷新时间(int)',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(10) NULL DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT 0 COMMENT '开关',
  `status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_test
-- ----------------------------
INSERT INTO `fa_test` VALUES (1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', '{\"a\":\"1\",\"b\":\"2\"}', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1499682285, 1499682526, 1499682526, NULL, 0, 1, 'normal', '1');

-- ----------------------------
-- Table structure for fa_user
-- ----------------------------
DROP TABLE IF EXISTS `fa_user`;
CREATE TABLE `fa_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '组别ID',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '格言',
  `money` decimal(10, 2) UNSIGNED NOT NULL COMMENT '余额',
  `score` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '积分',
  `successions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '连续登录天数',
  `maxsuccessions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '最大连续登录天数',
  `prevtime` int(10) NULL DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) NULL DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `joinip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) NULL DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Token',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  `verification` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user
-- ----------------------------
INSERT INTO `fa_user` VALUES (1, 1, 'admin', 'admin', 'c13f62012fd6a8fdf06b3452a94430e5', 'rpR6Bv', 'admin@163.com', '13888888888', '/uploads/20200829/3ab04b7b18bc70c20bde7e72d51be817.jpg', 0, 0, '2017-04-15', '', 0.00, 0, 1, 1, 1516170492, 1516171614, '127.0.0.1', 0, '127.0.0.1', 1491461418, 0, 1598676616, '', 'normal', '');

-- ----------------------------
-- Table structure for fa_user_group
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_group`;
CREATE TABLE `fa_user_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '权限节点',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_group
-- ----------------------------
INSERT INTO `fa_user_group` VALUES (1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1515386468, 1516168298, 'normal');

-- ----------------------------
-- Table structure for fa_user_money_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_money_log`;
CREATE TABLE `fa_user_money_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `money` decimal(10, 2) NOT NULL COMMENT '变更余额',
  `before` decimal(10, 2) NOT NULL COMMENT '变更前余额',
  `after` decimal(10, 2) NOT NULL COMMENT '变更后余额',
  `memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员余额变动表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_money_log
-- ----------------------------

-- ----------------------------
-- Table structure for fa_user_rule
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_rule`;
CREATE TABLE `fa_user_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) NULL DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) NULL DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NULL DEFAULT 0 COMMENT '权重',
  `status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_rule
-- ----------------------------
INSERT INTO `fa_user_rule` VALUES (1, 0, 'index', '前台', '', 1, 1516168079, 1516168079, 1, 'normal');
INSERT INTO `fa_user_rule` VALUES (2, 0, 'api', 'API接口', '', 1, 1516168062, 1516168062, 2, 'normal');
INSERT INTO `fa_user_rule` VALUES (3, 1, 'user', '会员模块', '', 1, 1515386221, 1516168103, 12, 'normal');
INSERT INTO `fa_user_rule` VALUES (4, 2, 'user', '会员模块', '', 1, 1515386221, 1516168092, 11, 'normal');
INSERT INTO `fa_user_rule` VALUES (5, 3, 'index/user/login', '登录', '', 0, 1515386247, 1515386247, 5, 'normal');
INSERT INTO `fa_user_rule` VALUES (6, 3, 'index/user/register', '注册', '', 0, 1515386262, 1516015236, 7, 'normal');
INSERT INTO `fa_user_rule` VALUES (7, 3, 'index/user/index', '会员中心', '', 0, 1516015012, 1516015012, 9, 'normal');
INSERT INTO `fa_user_rule` VALUES (8, 3, 'index/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 4, 'normal');
INSERT INTO `fa_user_rule` VALUES (9, 4, 'api/user/login', '登录', '', 0, 1515386247, 1515386247, 6, 'normal');
INSERT INTO `fa_user_rule` VALUES (10, 4, 'api/user/register', '注册', '', 0, 1515386262, 1516015236, 8, 'normal');
INSERT INTO `fa_user_rule` VALUES (11, 4, 'api/user/index', '会员中心', '', 0, 1516015012, 1516015012, 10, 'normal');
INSERT INTO `fa_user_rule` VALUES (12, 4, 'api/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 3, 'normal');

-- ----------------------------
-- Table structure for fa_user_score_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_score_log`;
CREATE TABLE `fa_user_score_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT 0 COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT 0 COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT 0 COMMENT '变更后积分',
  `memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员积分变动表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_score_log
-- ----------------------------
INSERT INTO `fa_user_score_log` VALUES (1, 1, 0, 0, 0, '管理员变更积分', 1598676616);

-- ----------------------------
-- Table structure for fa_user_token
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_token`;
CREATE TABLE `fa_user_token`  (
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Token',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(10) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员Token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_token
-- ----------------------------

-- ----------------------------
-- Table structure for fa_version
-- ----------------------------
DROP TABLE IF EXISTS `fa_version`;
CREATE TABLE `fa_version`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `oldversion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '包大小',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '强制更新',
  `createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '版本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_version
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
